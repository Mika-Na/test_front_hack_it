import { ref } from "vue";
import { Equipment } from './typeData';
import { teachers, mentions, courses } from './allData';
import axios from "axios";

export const showModalDialogue = ref<boolean>(false);
export const typeQuestion = ref<string>('');
export const typeModal = ref<string>('');
// export const selected = ref<Mention | Teacher | Course | Room | Classroom | Subject>();
// export const showModal = ref<boolean>(false);
// export const selectedMention = ref<Mention>({ id: '', name: '', abrev: '' });
// export const selectedTeacher = ref<Teacher>({ id: '', name: '', lastName: '', tel: '', address: '' });
// export const selectedClassroom = ref<Classroom>({ id: '', idCourse: '', nbrGroup: 0, nbrStudent: 0, semester: '', idMention: '' });
// export const selectedRoom = ref<Room>({ id: '', type: '', name: '', capacity: 0, abrev: '' });
// export const selectedCourse = ref<Course>({ id: '', name: '', abrev: '', idMention: '' });
// export const selectedS = ref<Subject>({ id: '', name: '', idTeacher: '', semester: '', idCourse: '' });

// cacher les formulaire
export const showAdd = ref<boolean>(true);
export const onHideAdd = (e: Event) => {
  let arrow = e.target as HTMLDivElement
  if (showAdd.value) {
    arrow.classList.add('rotate')
    showAdd.value = !showAdd.value
  }
  else {
    arrow.classList.remove('rotate')
    showAdd.value = !showAdd.value
  }
}

// affiche les modale
export const onShowModal = (type: string, idC: string, ElementFind: Room[] | Classroom[] | Teacher[] | Mention[] | Subject[]) => {
  if (type == 'delete') {
    showModalDialogue.value = true
    typeModal.value = 'question'
    typeQuestion.value = 'supprimer'
  }
  else if (type === 'success' || type === 'error') {
    typeModal.value = type
    showModalDialogue.value = true
  }
  else if(type === 'updateQuestion'){
    typeQuestion.value = 'modifier'
    showModalDialogue.value = true
    typeModal.value = 'question'
    showModal.value = false
  }
  else {
    showModal.value = true
    typeModal.value = type
  }
  let elementSelect = ElementFind.find(({ id }) => id === idC)
  if (elementSelect) {
    selected.value = elementSelect
    if ('tel' in elementSelect) {
      selectedTeacher.value = elementSelect
    }
    if ('capacity' in elementSelect) {
      selectedRoom.value = elementSelect
    }
    if ('nbrStudent' in elementSelect) {
      selectedClassroom.value = elementSelect
    }
    if ('idMention' in elementSelect && 'abrev' in elementSelect && 'name' in elementSelect) {
      selectedCourse.value = elementSelect as Course;
    }
    if ('idTeacher' in elementSelect) {
      selectedS.value = elementSelect
    }
    else {
      selectedMention.value = elementSelect as Mention
    }
  }
}

const noTeacher = ref<Teacher>({
  address: "",
  id: '',
  lastName: '',
  name: '',
  tel: ''
})
const noMention = ref<Mention>({
  abrev: '',
  id: '',
  name: ''
})
const noCourse = ref<Course>({
  id: '',
  name: '',
  abrev: '',
  idMention: '',
})

// pour l'affichage du matiere et parcoure
export const getTeacher = (idT: string) => {
  let teacherFind = teachers.value.find(({ id: idTeacher }) => idTeacher === idT)
  if (teacherFind)
    return teacherFind
  else return noTeacher.value
}

export const getMention = (idM: string) => {
  let mentionFind = mentions.value.find(({ id }) => id === idM)
  if (mentionFind)
    return mentionFind
  else return noMention.value
}
export const getCourse = (idC: string) => {
  let cours = courses.value.find(({ id }) => id === idC)
  if (cours) {
    return cours
  }
  else return noCourse.value
}

export const onUpdate = (nameUptade: string, params: Room | Classroom | Teacher | Mention | Subject) => {

  axios.put(`/api/update${nameUptade}`, params).then((response) => {
    if (response.data === 'success') {
      showModal.value = false
      typeModal.value = 'success'
      showModalDialogue.value = true
    }
    else{
      showModal.value = false
      typeModal.value = 'error'
      showModalDialogue.value = true
    }
  })
}
export const onDelete = (nameDelete : string , id : string)=>{
  axios.delete(`/api/delete${nameDelete}/${id}`).then((response)=>{
    if (response.data === 'success') {
      showModal.value = false
      typeModal.value = 'success'
      showModalDialogue.value = true
    }
    else{
      showModal.value = false
      typeModal.value = 'error'
      showModalDialogue.value = true
    }
  })
}


// export const onAdd = (nameAdd: string, params: Room | Classroom | Teacher | Mention | Subject) => {
//   axios.put(`/api/add${nameAdd}`, params).then((response) => {
//     if (response.data === 'success') {
//       showModal.value = false
//       typeModal.value = 'success'
//       showModalDialogue.value = true
//     }
//   })
// }