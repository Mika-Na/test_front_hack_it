import { createRouter, createWebHistory } from 'vue-router';
import Home from '../components/Home/Home.vue'
import Shop from '../components/Page/Shop/Shop.vue';
import Sale from '../components/Page/Sale/Sale.vue';
import FormeVue from '../components/Page/addDetails/addDetails.vue';
import Modale from '../components/Page/Modale/Modale.vue';
import Login from '../components/Login/Login.vue';
// import Enregistrer from '../components/Enregistrer/Enregistrer.vue';

export default createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/shop',
      name: 'Shop',
      component: Shop,
    },
    {
      path: '/sale',
      name: 'sale',
      component: Sale,
    },
    {
      path: '/from',
      name: 'from',
      component: FormeVue,
    },
    {
      path : '/modale',
      name : 'modale',
      component : Modale
    },
    {
      path : '/login',
      name : 'login',
      component : Login
    },
  ]
})