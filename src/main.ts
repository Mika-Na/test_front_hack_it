import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from './Router/Router';
import '@fortawesome/fontawesome-free/css/all.css';
import 'bootstrap/dist/css/bootstrap.css';
createApp(App).use(router).mount('#app')
