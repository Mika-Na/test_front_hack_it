export type user = {
  name : string,
  password : string
}
export type Product = {
  id: number,
  nameProduct: string,
 
} 
export type presentation = {
  id:string, 
  tittle: string,
  movie: string,
  detail: string
} 
export type Equipment = {
  id : string,
  nom : string,
  description : string,
  prix : number,
  capacite : number
} 
export type materiel = {
  id : string,
  nom : string,
  consomation : string,
  quantity:number,
  
} 
export type accessoire = {
  id : string,
  nom : string,
  type : string,
  prix : number,
  equipement_id : string
}